<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("_partials/header.php") ?>
	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/menu.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="main-content">
		<div class="page-content">
			<div class="container-fluid">

				<!-- start page title -->
				<div class="row">
					<div class="col-12">
						<div class="page-title-box d-sm-flex align-items-center justify-content-between">
							<h4 class="mb-sm-0 font-size-18">Dashboard</h4>

							<div class="page-title-right">
								<ol class="breadcrumb m-0">
									<li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
									<li class="breadcrumb-item active"></li>
								</ol>
							</div>

						</div>
					</div>
				</div>
				<!-- end page title -->
				<div class="row">

					<div class="col-xl-12">
						<div class="row">
							<div class="col-lg-4">
								<div class="card mini-stats-wid">
									<div class="card-body">

										<div class="d-flex flex-wrap">
											<div class="me-3">
												<p class="text-muted mb-2">Data Karyawan</p>
												<h5 class="mb-0">750</h5>
											</div>

											<div class="avatar-sm ms-auto">
												<div class="avatar-title bg-light rounded-circle text-primary font-size-20">
													<i class="bx bxs-book-bookmark"></i>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="card blog-stats-wid">
									<div class="card-body">

										<div class="d-flex flex-wrap">
											<div class="me-3">
												<p class="text-muted mb-2">Data Renewal Kontrak</p>
												<h5 class="mb-0">50</h5>
											</div>

											<div class="avatar-sm ms-auto">
												<div class="avatar-title bg-light rounded-circle text-primary font-size-20">
													<i class="bx bxs-note"></i>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card blog-stats-wid">
									<div class="card-body">
										<div class="d-flex flex-wrap">
											<div class="me-3">
												<p class="text-muted mb-2">Data User</p>
												<h5 class="mb-0">4</h5>
											</div>

											<div class="avatar-sm ms-auto">
												<div class="avatar-title bg-light rounded-circle text-primary font-size-20">
													<i class="bx bxs-message-square-dots"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end row -->
					</div>
					<!-- end col -->



				</div>
			</div>
		</div>
</body>

<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>

</html>
