<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

	<div data-simplebar class="h-100">

		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<!-- Left Menu Start -->
			<ul class="metismenu list-unstyled" id="side-menu">
				<li class="menu-title" key="t-menu">Menu</li>

				<li>
					<a href="<?php echo site_url('dashboard') ?>" class="waves-effect">
						<i class="bx bx-home-circle"></i>
						<span key="t-dashboards">Dashboards</span>
					</a>
				</li>

				<li>
					<a href="javascript: void(0);" class="has-arrow waves-effect">
						<i class="bx bx-layout"></i>
						<span key="t-layouts">Data Master</span>
					</a>
					<ul class="sub-menu" aria-expanded="true">
						<li>
							<a href="<?php echo site_url('hod/list') ?>">Data HOD</a>
						</li>
						<li>
							<a href="<?php echo site_url('dept/list') ?>">Data Departemen</a>
						</li>
						<li>
							<a href="<?php echo site_url('position/list') ?>">Data Posisi</a>
						</li>
						<li>
							<a href="<?php echo site_url('user/list') ?>">Data Admin</a>
						</li>
					</ul>
				</li>

				<li class="menu-title" key="t-apps">App</li>

				<li>
					<a href="<?php echo site_url('employee/list') ?>" class="waves-effect">
						<i class="bx bx-layout"></i>
						<span key="t-layouts">Data Karyawan</span>
					</a>
				</li>
				<li>
					<a href="javascript: void(0);" class="has-arrow waves-effect">
						<i class="bx bx-layout"></i>
						<span key="t-layouts">Kategori Karyawan</span>
					</a>
					<ul class="sub-menu" aria-expanded="true">
						<li>
							<a href="#">Running</a>
						</li>
						<li>
							<a href="#">Finish Contract</a>
						</li>
						<li>
							<a href="#">Resign</a>
						</li>
						<li>
							<a href="#">Run Away</a>
						</li>
					</ul>
				</li>
				<li>

					<a href="#" class="waves-effect">
						<i class="bx bx-layout"></i>
						<span key="t-layouts">Renewal Kontrak</span>
					</a>
				</li>

				<li class="menu-title" key="t-apps">Ext</li>

				<li>
					<a href="javascript: void(0);" class="has-arrow waves-effect">
						<i class="bx bx-layout"></i>
						<span key="t-layouts">Management Data</span>
					</a>
					<ul class="sub-menu" aria-expanded="true">
						<li>
							<a href="#">Export</a>
						</li>
						<li>
							<a href="#">Import</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>
		<!-- Sidebar -->
	</div>
</div>
<!-- Left Sidebar End -->

</div>
</body>
