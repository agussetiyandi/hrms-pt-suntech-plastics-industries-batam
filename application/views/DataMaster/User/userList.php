<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("_partials/header.php") ?>
	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/menu.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="main-content">
		<div class="page-content">
			<div class="container-fluid">

				<!-- start page title -->
				<div class="row">
					<div class="col-12">
						<div class="page-title-box d-sm-flex align-items-center justify-content-between">
							<h4 class="mb-sm-0 font-size-18">Data HOD</h4>

							<div class="page-title-right">
								<ol class="breadcrumb m-0">
									<li class="breadcrumb-item"><a href="javascript: void(0);">Data master</a></li>
									<li class="breadcrumb-item active">List HOD</li>
								</ol>
							</div>

						</div>
					</div>
				</div>
				<!-- end page title -->

				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
          
								<!-- <h4 class="card-title">Default Datatable</h4>
								<p class="card-title-desc">DataTables has most features enabled by
									default, so all you need to do to use it with your own tables is to call
									the construction function: <code>$().DataTable();</code>.
								</p> -->
								<div class="card col-2">
								<button type="button" class="btn btn-success waves-effect btn-label waves-light"><i
										class="bx bx-plus label-icon"></i> Tambah Data</button>
								</div>

								<table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
									<thead>
										<tr>
											<th style="text-align:center" width="5%">No</th>
											<th style="text-align:center">Nama</th>
                      <th style="text-align:center">Email</th>
                      <th style="text-align:center">Username</th>
                      <th style="text-align:center">Password</th>
                      <th style="text-align:center">Level</th>
											<th style="text-align:center" width="20%">Date Modified</th>
											<th style="text-align:center" width="10%"></th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
											foreach($list_user->result() as $row) :
										?>
										<tr>
											<td align="center"><?= $no++; ?></td>
											<td><?= $row->name ?></td>
                      <td><?= $row->email ?></td>
                      <td><?= $row->username ?></td>
                      <td>********</td>
                      <td><?= $row->level ?></td>
											<td align="center"><?= $row->date_modified ?></td>
											<td align="center">
												<a href="#" type="button" class="btn btn-warning btn-sm waves-effect waves-light">
													<i class="fa fa-edit"></i></a>
												<a href="#" type="button" class="btn btn-danger btn-sm waves-effect waves-light">
													<i class="fa fa-trash"></i></a>
											</td>
										</tr>
										<?php
											endforeach;
										?>
									</tbody>
								</table>


							</div>
						</div>
					</div> <!-- end col -->
				</div> <!-- end row -->

			</div>
		</div>
</body>

<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>

<script type="text/javascript">
	$("#datatable").DataTable();

</script>

<script type="text/javascript">
	$("#datatable").DataTable();

	function deleteConfirm(position_id) {
		console.log('buka modal');
		$("#deletePositionId").html(position_id);
		$("#linkdelete").prop("href", "<?php echo site_url('Position/positionDelete/') ?>" + position_id);
		$("#deleteConfirmModal").modal("show");
	}

</script>

</html>
