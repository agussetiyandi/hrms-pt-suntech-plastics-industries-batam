<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("_partials/header.php") ?>
	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/menu.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="main-content">
		<div class="page-content">
			<div class="container-fluid">

				<!-- start page title -->
				<div class="row">
					<div class="col-12">
						<div class="page-title-box d-sm-flex align-items-center justify-content-between">
							<h4 class="mb-sm-0 font-size-18">Data Karyawan</h4>
							<div class="page-title-right">
								<ol class="breadcrumb m-0">
									<li class="breadcrumb-item"><a href="javascript: void(0);">Data master</a></li>
									<li class="breadcrumb-item active">List Karyawan</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
				<!-- end page title -->

				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">DATA PELAMAR KERJA</h4>
								<p class="card-title-desc"><i>Work Employee Data</i></p>
								<?php echo form_open_multipart('Employee/addNewEmployee'); ?>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="badge_number">No. Karyawan</label>
											<input id="badge_number" name="badge_number" type="text" class="form-control"
												onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">Departemen</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<?php
                      		foreach($list_dept as $ld) {
                    		?>
												<option value="<?=$ld->dept_name;?>"><?=$ld->dept_name;?></option>
												<?php
                      		}
                    		?>
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">Posisi</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<?php
                      		foreach($list_position as $lp) {
                    		?>
												<option value="<?=$lp->position_name;?>"><?=$lp->position_name;?></option>
												<?php
                      		}
                    		?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="name">Nama Lengkap</label>
											<input id="name" name="name" type="text" class="form-control"
												onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="date_join">Tanggal Masuk</label>
											<input id="date_join" name="date_join" type="date" class="form-control">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="phone_number">No. Telp</label>
											<input id="phone_number" name="phone_number" type="text" class="form-control"
												onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="jamsostek_number">No. BPJS Ketenagakerjaan</label>
											<input id="jamsostek_number" name="jamsostek_number" type="text" class="form-control"
												onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="address">Alamat</label>
											<textarea class="form-control" onkeyup="this.value = this.value.toUpperCase()" id="address"
												name="address" rows="2"></textarea placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="npwp_number">No. NPWP</label>
											<input id="npwp_number" name="npwp_number" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-body">
								<h4 class="card-title">DATA DIRI / PERSONAL</h4>
								<p class="card-title-desc"><i>Personal Data</i></p>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="place_of_birth">Tempat Lahir</label>
											<input id="place_of_birth" name="place_of_birth" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="date_of_birth">Tanggal Lahir</label>
											<input id="date_of_birth" name="date_of_birth" type="date" class="form-control">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="ktp_number">No. KTP</label>
											<input id="ktp_number" name="ktp_number" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="age">Umur</label>
											<input id="age" name="age" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label class="control-label">Jenis Kelamin</label>
											<select name="emp_gender" class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="LAKI-LAKI">LAKI-LAKI</option>
												<option value="PEREMPUAN">PEREMPUAN</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label class="control-label">Agama</label>
											<select name="emp_gender" class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="ISLAM">ISLAM</option>
												<option value="PROTESTAN">PROTESTAN</option>
												<option value="KATOLIK">KATOLIK</option>
												<option value="HINDU">HINDU</option>
												<option value="BUDDHA">BUDDHA</option>
												<option value="KHONGHUCU">KHONGHUCU</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="hobby">Hobi</label>
											<input id="hobby" name="hobby" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label class="control-label">Status Perkawinan</label>
											<select name="emp_gender" class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="SINGLE">SINGLE</option>
                    		<option value="MENIKAH">MENIKAH</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">PENDIDIKAN</h4>
								<p class="card-title-desc"><i>Education</i></p>
								<div class="row">
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Pendidikan Akhir / Kursus</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Tahun</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Gelar yang diperolah</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Jurusan</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">PENGALAMAN KERJA</h4>
								<p class="card-title-desc"><i>Work Experience</i></p>
								<div class="row">
									<div class="col-sm-1">
										<div class="mb-3">
											<label for="productname">Tahun</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Nama Perusahaan</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Jabatan</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Gaji Pokok/Sallary</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Alasan Berhenti Bekerja</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-1">
										<div class="mb-3">
											<label for="productname">Tahun</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Nama Perusahaan</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Jabatan</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Gaji Pokok/Sallary</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Alasan Berhenti Bekerja</label>
											<input id="productname" name="productname" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">KELUARGA YANG BISA DIHUBUNGI DALAM KEADAAN DARURAT</h4>
								<p class="card-title-desc"><i>Familly on Emergency</i></p>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="urgent_contact_name">Nama</label>
											<input id="urgent_contact_name" name="urgent_contact_name" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="urgent_contact_phone_number">No. Telp</label>
											<input id="urgent_contact_phone_number" name="urgent_contact_phone_number" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="urgent_contact_address">Alamat</label>
											<input id="urgent_contact_address" name="urgent_contact_address" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div><br>
									<div>
                		<button type="submit" class="btn btn-primary w-md">Submit</button>
                	</div>
							</div>
						</div>


					</div>
				</div>
				<!-- end row -->

			</div>
		</div>
</body>

<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>

</html>
