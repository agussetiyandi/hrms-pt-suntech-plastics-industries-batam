<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("_partials/header.php") ?>
	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/menu.php") ?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="main-content">
		<div class="page-content">
			<div class="container-fluid">

				<!-- start page title -->
				<div class="row">
					<div class="col-12">
						<div class="page-title-box d-sm-flex align-items-center justify-content-between">
							<h4 class="mb-sm-0 font-size-18">Data Karyawan</h4>
							<div class="page-title-right">
								<ol class="breadcrumb m-0">
									<li class="breadcrumb-item"><a href="javascript: void(0);">Data master</a></li>
									<li class="breadcrumb-item active">List Karyawan</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
				<!-- end page title -->

				<div class="row">
					<div class="col-12">
						<?php foreach($employee_data->result() as $row) : ?>
						<?php echo form_open_multipart('Employee/employeeUpdateData'); ?>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">DATA PELAMAR KERJA</h4>
								<p class="card-title-desc"><i>Work Employee Data</i></p>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="name">Nama Lengkap</label>
											<input type="hidden" name="employee_id" value="<?= $row->employee_id ?>">
											<input id="name" name="name" type="text" class="form-control" value="<?= $row->name ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">No. Karyawan</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->badge_number ?>"  onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">Tanggal Masuk</label>
											<input id="productname" name="productname" type="date" class="form-control" value="<?= $row->date_join ?>"  onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">Kontrak Berakhir</label>
											<input id="productname" name="productname" type="date" class="form-control" value="<?= $row->finish_contract ?>"  onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">Departemen</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<?php
                      		foreach($list_position as $lp) {
                    		?>
												<option value="<?=$lp->position_name;?>"><?=$lp->position_name;?></option>
												<?php
                      		}
                    		?>
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">Posisi</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<?php
                      		foreach($list_position as $lp) {
                    		?>
												<option value="<?=$lp->position_name;?>"><?=$lp->position_name;?></option>
												<?php
                      		}
                    		?>
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">HOD</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<?php
                      		foreach($list_position as $lp) {
                    		?>
												<option value="<?=$lp->position_name;?>"><?=$lp->position_name;?></option>
												<?php
                      		}
                    		?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">No. BPJS Ketenagakerjaan</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->jamsostek_number ?>"  onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">No. BPJS Kesehatan</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->health_bpjs_number ?>"  onkeyup="this.value = this.value.toUpperCase()">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Medical By</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->bpjs_medical_by ?>"  onkeyup="this.value = this.value.toUpperCase()">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">No. Telp</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->phone_number ?>"  onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">No. Whatsapp</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->whatsapp_number ?>"  onkeyup="this.value = this.value.toUpperCase()">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Email</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->email ?>"  onkeyup="this.value = this.value.toUpperCase()">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">No. NPWP</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->npwp_number ?>"  onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">No. Rekening BCA</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->account_rek_bca ?>"  onkeyup="this.value = this.value.toUpperCase()">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Alamat</label>
											<input id="productname" name="productname" type="text" class="form-control" value="<?= $row->address ?>"  onkeyup="this.value = this.value.toUpperCase()">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">Status Kontrak</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="KONTRAK">KONTRAK</option>
												<option value="PERMANEN">PERMANEN</option>
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label class="control-label">Status Karyawan</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="RUNNING">RUNNING</option>
												<option value="FINISH CONTRACT">FINISH CONTRACT</option>
												<option value="RESIGN">RESIGN</option>
												<option value="RUN AWAY">RUN AWAY</option>
											</select>
										</div>	
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Last Date</label>
											<input id="productname" name="productname" type="date" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-body">
								<h4 class="card-title">DATA DIRI / PERSONAL</h4>
								<p class="card-title-desc"><i>Personal Data</i></p>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">Tempat Lahir</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">Tanggal Lahir</label>
											<input id="productname" name="productname" type="date" class="form-control">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">No. KTP</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">Umur</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label class="control-label">Jenis Kelamin</label>
											<select class="form-control select2">
												<option>Select</option>
												<option value="FA">Fashion</option>
												<option value="EL">Electronic</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label class="control-label">Agama</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="FA">Fashion</option>
												<option value="EL">Electronic</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3">
											<label for="productname">Hobi</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3">
											<label class="control-label">Status Perkawinan</label>
											<select class="form-control select2">
												<option disabled selected>-- Pilih --</option>
												<option value="FA">Fashion</option>
												<option value="EL">Electronic</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">PENDIDIKAN</h4>
								<p class="card-title-desc"><i>Education</i></p>
								<div class="row">
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Pendidikan Akhir / Kursus</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Tahun</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Gelar yang diperolah</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Jurusan</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">PENGALAMAN KERJA</h4>
								<p class="card-title-desc"><i>Work Experience</i></p>
								<div class="row">
									<div class="col-sm-1">
										<div class="mb-3">
											<label for="productname">Tahun</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Nama Perusahaan</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Jabatan</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Gaji Pokok/Sallary</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Alasan Berhenti Bekerja</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-1">
										<div class="mb-3">
											<label for="productname">Tahun</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="productname">Nama Perusahaan</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Jabatan</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="mb-3">
											<label for="productname">Gaji Pokok/Sallary</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="mb-3">
											<label for="productname">Alasan Berhenti Bekerja</label>
											<input id="productname" name="productname" type="text" class="form-control" placeholder=". . . .">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h4 class="card-title">KELUARGA YANG BISA DIHUBUNGI DALAM KEADAAN DARURAT</h4>
								<p class="card-title-desc"><i>Familly on Emergency</i></p>
								<div class="row">
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="urgent_contact_name">Nama</label>
											<input id="urgent_contact_name" name="urgent_contact_name" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="urgent_contact_phone_number">No. Telp</label>
											<input id="urgent_contact_phone_number" name="urgent_contact_phone_number" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . ."	>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3">
											<label for="urgent_contact_address">Alamat</label>
											<input id="urgent_contact_address" name="urgent_contact_address" type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder=". . . .">
										</div>
									</div>
								</div><br>
									<div>
                		<button type="submit" class="btn btn-primary w-md">Submit</button>
                	</div>
							</div>
						</div>
						<?php echo form_close(); ?>
						<?php
							endforeach;
						?>
					</div>
				</div>
				<!-- end row -->



			</div>
		</div>
</body>

<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>

</html>
