<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
	<meta charset="utf-8" />
	<title>Login | PT. Suntech Plastics Industries Batam</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- App favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">
	<!-- Bootstrap Css -->
  <?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<div class="account-pages my-5 pt-sm-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 col-lg-6 col-xl-5">
					<div class="card overflow-hidden">
						<div class="bg-primary bg-soft">
							<div class="row">
								<div class="col-7">
									<div class="text-primary p-4">
										<h5 class="text-primary">Welcome Back !</h5>
										<p>Sign in to continue.</p>
									</div>
								</div>
								<div class="col-5 align-self-end">
									<img src="assets/images/profile-img.png" alt="" class="img-fluid">
								</div>
							</div>
						</div>
						<div class="card-body pt-0">
							<div class="auth-logo">
								<!-- <a href="index.html" class="auth-logo-light">
									<div class="avatar-md profile-user-wid mb-4">
										<span class="avatar-title rounded-circle bg-light">
											<img src="assets/images/logo-light.svg" alt="" class="rounded-circle" height="34">
										</span>
									</div>
								</a> -->

								<a href="index.html" class="auth-logo-dark">
									<div class="avatar-md profile-user-wid mb-4">
										<span class="avatar-title rounded-circle bg-light">
											<img src="assets/images/logo.svg" alt="" class="rounded-circle" height="34">
										</span>
									</div>
								</a>
							</div>
							<div class="p-2">
								<form class="form-horizontal" action="<?php echo base_url('login/aksi_login'); ?>" method="post">
									<?php if($this->session->flashdata('msg') != NULL){?>
									<div class="alert alert-danger">
										<?php echo $this->session->flashdata('msg');?>
									</div>
									<?php } ?>

									<div class="mb-3">
										<label for="username" class="form-label">Username</label>
										<input type="text" class="form-control" name="username" id="username" placeholder="Enter username">
									</div>

									<div class="mb-3">
										<label for="password" class="form-label">Password</label>
										<input type="text" class="form-control" name="password" id="password" placeholder="Enter password">
									</div>

									<!-- <div class="mb-3">
										<label class="form-label">Password</label>
										<div class="input-group auth-pass-inputgroup">
											<input type="password" class="form-control" placeholder="Enter password" aria-label="Password"
												aria-describedby="password-addon">
											<button class="btn btn-light " type="button" id="password-addon"><i
													class="mdi mdi-eye-outline"></i></button>
										</div>
									</div> -->

									<div class="form-check">
										<input class="form-check-input" type="checkbox" id="remember-check">
										<label class="form-check-label" for="remember-check">
											Remember me
										</label>
									</div>

									<div class="mt-3 d-grid">
										<button class="btn btn-primary waves-effect waves-light" name="submit" type="submit">Log In</button>
									</div>

									<div class="mt-4 text-center">
										<a href="auth-recoverpw.html" class="text-muted"><i class="mdi mdi-lock me-1"></i> Forgot your
											password?</a>
									</div>
								</form>
							</div>

						</div>
					</div>
					<div class="mt-5 text-center">

						<div>
							<p>Copyright © <script>
									document.write(new Date().getFullYear())

								</script> - PT. Suntech Plastics Industries Batam</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- end account-pages -->

	<!-- JAVASCRIPT -->
  <?php $this->load->view("_partials/js.php") ?>

</body>
</html>
