<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dept extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("Dept_model"); 
	}

	public function list() {
		$data['list_dept'] = $this->Dept_model->get_all();
		$this->load->view('DataMaster/Departement/deptList', $data);
	}

}