<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hod extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("Hod_model"); 
	}

	public function list() {
		$data['list_hod'] = $this->Hod_model->get_all();
		$this->load->view('DataMaster/Hod/hodList', $data);
	}

}