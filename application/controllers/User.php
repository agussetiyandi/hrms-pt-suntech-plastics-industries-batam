<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("User_model"); 
	}

	public function list() {
		$data['list_user'] = $this->User_model->get_all();
		$this->load->view('DataMaster/User/userList', $data);
	}

}