<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("Employee_model"); 
	}

	public function list() {
		$data['list_employee'] = $this->Employee_model->get_all();
		$this->load->view('DataMaster/Employee/employeeList', $data);
	}

	public function add()
	{
		$data['list_employee'] = $this->Employee_model->get_all();
		$this->load->view('DataMaster/Employee/employeeCreate',$data);
    }

    public function update($employee_id)
    {
    	$data = array(
    		'employee_data' => $this->Employee_model->get_employee($employee_id),
    	);
		$this->load->view('DataMaster/Employee/employeeUpdate', $data);
    }
	
		function addNewEmployee()
		{
	  	$config['upload_path']          = './gambar/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['overwrite']			= true;
			$config['overwrite']			= true;

			$this->load->library('upload',$config);
			$this->upload->do_upload('picture');

			$upload_data = $this->upload->data(); 
  		$file_name =   $upload_data['file_name'];

			$data = array(
				'badge_number' => $this->input->post('badge_number'),
				'name' => $this->input->post('name'),
				'position_id' => $this->input->post('position_id'),
				'dept_id' => $this->input->post('dept_id'),
				'hod_id' => $this->input->post('hod_id'),
				'date_join' => $this->input->post('date_join'),
				'finish_contract' => $this->input->post('finish_contract'),
				'bank_rek_bca' => $this->input->post('bank_rek_bca'),
				'jamsostek_number' => $this->input->post('jamsostek_number'),
				'npwp_number' => $this->input->post('npwp_number'),
				'religion' => $this->input->post('religion'),
				'address' => $this->input->post('address'),
				'phone_number' => $this->input->post('phone_number'),
				'img' => $file_name
			);
			$this->Employee_model->employeeInsertDB($data);
			redirect('employee/list');
		}	

		function employeeUpdateData(){
			$config['upload_path']          = './gambar/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['overwrite']			= true;
			$config['overwrite']			= true;
	
			$this->load->library('upload',$config);
			$this->upload->do_upload('picture');
	
			$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
	
			$data = array(
				'badge_number' => $this->input->post('badge_number'),
				'name' => $this->input->post('name'),
				'position_id' => $this->input->post('position_id'),
				'dept_id' => $this->input->post('dept_id'),
				'hod_id' => $this->input->post('hod_id'),
				'date_join' => $this->input->post('date_join'),
				'finish_contract' => $this->input->post('finish_contract'),
				'bank_rek_bca' => $this->input->post('bank_rek_bca'),
				'jamsostek_number' => $this->input->post('jamsostek_number'),
				'npwp_number' => $this->input->post('npwp_number'),
				'religion' => $this->input->post('religion'),
				'address' => $this->input->post('address'),
				'phone_number' => $this->input->post('phone_number'),
				'img' => $file_name
			);
			$condition['employee_id'] = $this->input->post('employee_id');
			$this->Employee_model->employeeUpdateDB($data, $condition);
			redirect('employee/list');
		}
	
		function employeeDelete($data){
			$this->Employee_model->employeeDeleteDB($data);
			redirect('DataMaster/Employee/employeeList');
		}

}