<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("Position_model"); 
	}

	public function list() {
		$data['list_position'] = $this->Position_model->get_all();
		$this->load->view('Datamaster/Position/positionList', $data);
	}

	// public function addPosition()
	// {
	// 	$data['list_cat'] = $this->Product_model->get_all_cat();
	// 	$this->load->view('addProduct',$data);
  //   }

  //   public function productUpdate($id_product)
  //   {
  //   	$data = array(
  //   		'product_data' => $this->Product_model->get_product($id_product),
  //   		'list_cat' => $this->Product_model->get_all_cat()
  //   	);
	// 	$this->load->view('productUpdate', $data);
  //   }

  //   public function colorUpdate($id_product)
  //   {
  //   	$data['list_color'] = $this->Product_model->get_color_one($id_product);
  //   	$data['id'] = $id_product;
	// 	$this->load->view('addColor', $data);
  //   }

  //   public function sizeUpdate($id_product)
  //   {
  //   	$data['list_size'] = $this->Product_model->get_size_one($id_product);
  //   	$data['id'] = $id_product;
	// 	$this->load->view('addSize', $data);
  //   }
  
	// public function productCategory()
	// {
	// 	$data['list_cat'] = $this->Product_model->get_all_cat(); 
	//     $this->load->view('productCategory', $data);
	// }

	// function addNewProduct(){
	//   	$config['upload_path']          = './gambar/';
	// 	$config['allowed_types']        = 'gif|jpg|png';
	// 	$config['overwrite']			= true;
	// 	$config['overwrite']			= true;

	// 	$this->load->library('upload',$config);
	// 	$this->upload->do_upload('picture');

	// 	$upload_data = $this->upload->data(); 
  // 		$file_name =   $upload_data['file_name'];

	// 	$data = array(
	// 		'product_name' => $this->input->post('name'),
	// 		'stock' => $this->input->post('stock'),
	// 		'product_category' => $this->input->post('category'),
	// 		'capital_price_usd' => $this->input->post('modal_eur'),
	// 		'price_rupiah' => $this->input->post('ws_rp'),
	// 		'additional_fee' => $this->input->post('add_fee'),
	// 		'detail_price_rupiah' => $this->input->post('retail_rp'),
	// 		'images' => $file_name
	// 	);
	// 	$id = $this->Product_model->productInsertDB($data);
	// 	$color = array(
	// 		'id_product' => $id,
	// 		'color' => $this->input->post('color')
	// 	);
	// 	$this->Product_model->productInsertColor($color);

	// 	$size = array(
	// 		'id_product' => $id,
	// 		'size' => $this->input->post('size')
	// 	);
	// 	$this->Product_model->productInsertSize($size);
	// 	redirect('Product/productList');
	// }

	// function addColor(){
	// 	$data = array(
	// 		'id_product' => $this->input->post('id_product'),
	// 		'color' => $this->input->post('color')
	// 	);
	// 	$this->Product_model->productInsertColor($data);
	// 	$id = $data['id_product'];
	// 	redirect('Product/colorUpdate/'.$id);
	// }

	// function addSize(){
	// 	$data = array(
	// 		'id_product' => $this->input->post('id_product'),
	// 		'size' => $this->input->post('size')
	// 	);
	// 	$this->Product_model->productInsertSize($data);
	// 	$id = $data['id_product'];
	// 	redirect('Product/sizeUpdate/'.$id);
	// }

	// function productUpdateData(){
	// 	$config['upload_path']          = './gambar/';
	// 	$config['allowed_types']        = 'gif|jpg|png';
	// 	$config['overwrite']			= true;
	// 	$config['overwrite']			= true;

	// 	$this->load->library('upload',$config);
	// 	$this->upload->do_upload('picture');

	// 	$upload_data = $this->upload->data(); 
  // 		$file_name =   $upload_data['file_name'];

	// 	$data = array(
	// 		'product_name' => $this->input->post('name'),
	// 		'stock' => $this->input->post('stock'),
	// 		'product_category' => $this->input->post('category'),
	// 		'capital_price_usd' => $this->input->post('modal_eur'),
	// 		'price_rupiah' => $this->input->post('ws_rp'),
	// 		'additional_fee' => $this->input->post('add_fee'),
	// 		'detail_price_rupiah' => $this->input->post('retail_rp'),
	// 		'size' => $this->input->post('size'),
	// 		'color' => $this->input->post('color'),
	// 		'images' => $file_name
	// 	);
	// 	$condition['id_product'] = $this->input->post('id_product');
	// 	$this->Product_model->productUpdateDB($data, $condition);
	// 	redirect('Product/productList');
	// }

	// function productDelete($data){
	// 	$this->Product_model->productDeleteDB($data);
	// 	redirect('Product/productList');
	// }

	// function colorDelete(){
	// 	$data = array(
	// 		'id_product' => $this->input->post('id_product'),
	// 		'color' => $this->input->post('color')
	// 	);
	// 	$this->Product_model->productDeleteColor($data);
	// 	$id = $data['id_product'];
	// 	redirect('Product/colorUpdate/'.$id);
	// }

	// function sizeDelete($data){
	// 	$data = array(
	// 		'id_product' => $this->input->post('id_product'),
	// 		'size' => $this->input->post('size')
	// 	);
	// 	$this->Product_model->productDeleteSize($data);
	// 	$id = $data['id_product'];
	// 	redirect('Product/sizeUpdate/'.$id);
	// }

	// public function catUpdate($id_category)
  //   {
  //   	$data['product_data'] = $this->Product_model->get_cat($id_category);
	// 	$this->load->view('catUpdate', $data);
  //   }

	// function catInsert(){
	// 	$data = array(
	// 		'product_category' => $this->input->post('category')
	// 	);
	// 	$this->Product_model->catInsertDB($data);
	// 	redirect('Product/productCategory');
	// }

	// function catDelete($data){
	// 	$this->Product_model->catDeleteDB($data);
	// 	redirect('Product/productCategory');
	// }
}

