<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select("*");
		$this->db->from("stc_employee");
		$this->db->order_by("employee_id", "desc");
		return $this->db->get();
	}

	// function get_color()
	// {
	// 	$this->db->select("*");
	// 	$this->db->from("product_color");

	// 	return $this->db->get();
	// }

	// function get_size()
	// {
	// 	$this->db->select("*");
	// 	$this->db->from("product_size");

	// 	return $this->db->get();
	// }

	//hoho
	// function get_color_one($id_product)
	// {
	// 	$this->db->where("id_product", $id_product);
	// 	$this->db->select("*");
	// 	$this->db->from("product_color");

	// 	return $this->db->get();
	// }

	// function get_size_one($id_product)
	// {
	// 	$this->db->where("id_product", $id_product);
	// 	$this->db->select("*");
	// 	$this->db->from("product_size");

	// 	return $this->db->get();
	// }
	//hihi

	function get_employee($employee_id)
	{
		$this->db->where("employee_id", $employee_id);
		$this->db->select("*");
		$this->db->from("stc_employee");

		return $this->db->get();
	}

	function employeeInsertDB($data){
		$this->db->insert("stc_employee", $data);
		return $id = $this->db->insert_id();
	}

	// function productInsertSize($data){
	// 	$this->db->insert("product_size", $data);
	// }

	// function productInsertColor($data){
	// 	$this->db->insert("product_color", $data);
	// }

	function employeeUpdateDB($data, $condition){
		$this->db->where($condition);
		$this->db->update("stc_employee", $data);
	}

	function employeeDeleteDB($data){
		$this->db->where("employee_id", $data);
		$this->db->delete("stc_employee");
	}

	// function productDeleteColor($data){
	// 	$this->db->where($data);
	// 	$this->db->delete("product_color");
	// }

	// function productDeleteSize($data){
	// 	$this->db->where($data);
	// 	$this->db->delete("product_size");
	// }

	// function get_all_cat()
	// {
	// 	$this->db->select("*");
	// 	$this->db->from("product_category");

	// 	return $this->db->get();
	// }

	// function get_cat($id_cat)
	// {
	// 	$this->db->where("id_category", $id_cat);
	// 	$this->db->select("*");
	// 	$this->db->from("product_category");

	// 	return $this->db->get();
	// }

	// function catInsertDB($data){
	// 	$this->db->insert("product_category", $data);
	// }

	// function catDeleteDB($data){
	// 	$this->db->where("id_category", $data);
	// 	$this->db->delete("product_category");
	// }
}
