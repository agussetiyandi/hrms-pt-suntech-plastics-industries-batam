<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dept_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select("*");
		$this->db->from("stc_departement");

		return $this->db->get();
	}

	function get_dept($dept_id)
	{
		$this->db->where("dept_id", $dept_id);
		$this->db->select("*");
		$this->db->from("stc_departement");

		return $this->db->get();
	}

	
}