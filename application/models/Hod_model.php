<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hod_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select("*");
		$this->db->from("stc_hod");

		return $this->db->get();
	}

	function get_hod($hod_id)
	{
		$this->db->where("hod_id", $hod_id);
		$this->db->select("*");
		$this->db->from("stc_hod");

		return $this->db->get();
	}

	
}