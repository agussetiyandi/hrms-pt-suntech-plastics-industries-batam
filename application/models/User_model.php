<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function get_all()
	{
		$this->db->select("*");
		$this->db->from("stc_user");

		return $this->db->get();
	}

	function get_user($user_id)
	{
		$this->db->where("user_id", $user_id);
		$this->db->select("*");
		$this->db->from("stc_user");

		return $this->db->get();
	}

	
}